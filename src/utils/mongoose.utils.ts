import { Mongoose, connect } from "mongoose";

export class MongooseUtils {

    public static openConnection(): Promise<Mongoose> {
        return connect(process.env.MONGO_URI, {
            auth: {
                username: process.env.MONGO_USER,
                password: process.env.MONGO_PASSWORD
            },
            authSource: "admin", // Collection des comptes utiliseurs
            autoCreate: true // Mongoose va creer les tables et les bases pour nous
        });
    }
}