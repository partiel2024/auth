import mongoose, { SchemaTypes } from "mongoose";
import { ISession } from "../../../definitions/sessions";

export const SessionSchema = new mongoose.Schema<ISession>({
    token: {
        type: SchemaTypes.String,
        required: true,
        unique: true
    },
    account: {
        type: SchemaTypes.ObjectId,
        required: true,
        ref: "Account"
    }
}, {
    versionKey: false,
    collection: "sessions",
    timestamps: true
});