import * as crypto from 'crypto';
import { ethers } from 'ethers';

export class SecurityUtils {
    static sha256(str: string): string {
        const hash = crypto.createHash("sha256");
        hash.update(str);
        return hash.digest("hex");
    }

    static randomToken(): string {
        return crypto.randomBytes(64).toString("hex");
    }

    static createNewWallet(): string[] | null {
        try {
            const wallet = ethers.Wallet.createRandom();
            return [wallet.address, wallet.privateKey];
        } catch (err) {
            console.error(err);
            return null;
        }
    }
}