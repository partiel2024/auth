import { Router, Request, Response, json } from 'express';
import { Model, Mongoose } from 'mongoose';
import { AccountSchema } from '../mongoose/schemas';
import { IAccount } from '../../definitions/accounts';
import { SecurityUtils } from '../../utils/security.utils';
import { ISession } from '../../definitions/sessions';
import { SessionSchema } from '../mongoose/schemas/session.schema';

export class Authcontroller {

    private connection: Mongoose;
    private accountModel: Model<IAccount>;
    private sessionModel: Model<ISession>;

    public constructor(connection: Mongoose) {
        this.connection = connection;
        this.accountModel = this.connection.model("Account", AccountSchema);
        this.sessionModel = this.connection.model("Session", SessionSchema);
    }

    async subscribe(req: Request, res: Response) {
        if (!req.body.username || !req.body.password) {
            res.status(400);
            return;
        }
        const [publicKey, privateKey] = SecurityUtils.createNewWallet();
        try {
            await this.accountModel.create({
                username: req.body.username,
                password: SecurityUtils.sha256(req.body.password),
                publicKey: publicKey,
                privateKey: privateKey
            });
            res.status(201).end();
        } catch (e) {
            res.status(409).end();
        }
    }

    async login(req: Request, res: Response) {
        if (!req.body.password || !req.body.username) {
            res.status(400);
            return;
        }
        const account = await this.accountModel.findOne({
            username: req.body.username,
            password: SecurityUtils.sha256(req.body.password)
        }).exec();
        if (!account) {
            res.status(401);
            return;
        }
        const token = SecurityUtils.randomToken();
        const session = await this.sessionModel.create({
            token,
            account
        });
        res.status(200).json(session);
    }

    async me(req: Request, res: Response) {
        const authorization = req.headers["authorization"]; // Authorization = Bearer $TOKEN
        if (!authorization) {
            res.status(401).end();
            return;
        }
        const parts = authorization.split(" ");
        if (parts.length !== 2 || parts[0] !== "Bearer") {
            res.status(401).end();
            return;
        }
        const token = parts[1];
        const session = await this.sessionModel.findOne({
            token
        }).populate('account').exec();
        res.json(session);
    }

    buildRoutes(): Router {
        const router = Router();
        router.post("/subscribe", json(), this.subscribe.bind(this));
        router.post("/login", json(), this.login.bind(this));
        router.get("/me", this.me.bind(this));
        return router;
    }

}