import { config } from "dotenv"
config()

import { launchAPI } from "./src/platforms/express";
import { MongooseUtils } from "@coralie.byr7777/lib";

async function main() {
    const mongo = await MongooseUtils.openConnection();
    launchAPI(mongo);
}

main().catch(console.error)