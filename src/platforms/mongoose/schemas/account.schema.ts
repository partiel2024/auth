import { Schema, SchemaTypes } from "mongoose";
import { IAccount } from "../../../definitions/accounts";

export const AccountSchema = new Schema<IAccount>({
    username: {
        type: SchemaTypes.String,
        required: true,
        unique: true
    },
    password: {
        type: SchemaTypes.String,
        required: true
    },
    privateKey: {
        type: SchemaTypes.String,
        required: true,
        unique: true
    },
    publicKey: {
        type: SchemaTypes.String,
        required: true,
        unique: true
    },
}, {
    versionKey: false, // Enleve __v des collections
    collection: 'accounts', // Nom de la collection
    timestamps: true // Permet d'ajouter createdAt et updatedAt
});