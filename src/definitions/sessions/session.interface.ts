import { IAccount } from "../accounts";

export interface ISession {
    token: string;
    account: string | IAccount;
}